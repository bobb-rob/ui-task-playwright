import { Page, expect } from "@playwright/test";
import helperMethods from "../utils/HelperMethods";

export class LoginPage {
    private page: Page;
    private loginButton: string;
    private username: string;
    private password: string;

    constructor(page: Page) {
        this.page = page;
        this.loginButton = 'input[data-test="login-button"]';
        this.username = 'input[data-test="username"]';
        this.password = 'input[data-test="password"]';
    }

    async launchURL(): Promise<void> {
        await helperMethods.navigateTo(this.page, "https://www.saucedemo.com/");
    }

    async login(username: string, password: string): Promise<void> {
        await helperMethods.fill(this.page, this.username, username);
        await helperMethods.fill(this.page, this.password, password);
        await helperMethods.click(this.page, this.loginButton);
        await expect(await this.page.title()).toEqual('Swag Labs');
    }
}

