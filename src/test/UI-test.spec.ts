import { chromium, expect, Page, test } from "@playwright/test";
import { InventoryPage } from "../page/Inventory.page";
import { LoginPage } from "../page/login.page";

test.describe("Inventory sorting test", () => {
    let page: Page;
    let inventoryPage: InventoryPage;
    let loginPage: LoginPage;

    test.beforeAll(async ({ browser }) => {
        page = await browser.newPage();
        inventoryPage = new InventoryPage(page);
        loginPage = new LoginPage(page);
    });

    test.beforeEach(async () => {
        await loginPage.launchURL();
        await loginPage.login("standard_user", "secret_sauce");
    });

    test.afterAll(async () => {
        await page.close();
    });

    test("Sort items by name ascending and descending", async () => {
        await inventoryPage.sortItemsByNameAsc();
        await inventoryPage.verifyItemsSortedByNameAsc();

        await inventoryPage.sortItemsByNameDesc();
        await inventoryPage.verifyItemsSortedByNameDesc();
    });
});